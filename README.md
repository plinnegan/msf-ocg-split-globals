### MSF-OCG Global Dissemination

##### Description
Script designed for the specific purpose of splitting global data and metadata into program specific versions.

The script updates data elements, option sets, tracked entity attributes, program rule variables, program rules and program indicators.
Then all the dataValues associated with the dataElements.

It also highlights the dashboard items that need to bu updated **but does not change them** as there are so few and updating dashboard
items is not straightforward it is far more time efficient to just highlight and change through the UI.

The last stage deletes the now redundant global option sets and data elements.

####Process

1. Run run.py to create new metadata
2. Run update_de_values.sql on the server
3. Run Analytics
4. Run review_dash_items.py to find which dash items require updating (then manually update)
5. Replace GL data element in Event Report:<br/>*Surgery - List of Patients with Diabetes Related Surgery (last 12 months)*
6. Delete or replace all DE in event reports demo favourite.
7. Run remove_global_objects.py to remove global data elements and option sets.
8. Run delete_gl_des.sql and delete_non_tea_gl_os.sql
