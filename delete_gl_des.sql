DELETE
FROM dataelementattributevalues deav
  USING dataelement de
WHERE
  de.dataelementid = deav.dataelementid AND
  de.name LIKE '%GL -%';


DELETE
FROM dataelementtranslations det
  USING dataelement de
WHERE
  de.dataelementid = det.dataelementid AND
  de.name LIKE '%GL -%';


DELETE
FROM dataelementgroupmembers degm
USING dataelement de
WHERE
  de.dataelementid = degm.dataelementid AND
  de.name LIKE '%GL -%';


DELETE
FROM programstagedataelement psde
  USING dataelement de
WHERE
    psde.dataelementid = de.dataelementid AND
    de.name LIKE '%GL -%';


DELETE
FROM dataelementusergroupaccesses deuga
  USING dataelement de
WHERE
    deuga.dataelementid = de.dataelementid AND
    de.name LIKE '%GL -%';


DELETE
FROM trackedentitydataelementdimension teded
  USING dataelement de
WHERE
    de.dataelementid = teded.dataelementid AND
    de.name LIKE '%GL -%';


DELETE
FROM programstagesection_dataelements pss_de
  USING dataelement de
WHERE
    de.dataelementid = pss_de.dataelementid AND
    de.name LIKE '%GL -%';


DELETE
FROM dataelement
WHERE name LIKE '%GL -%';