DELETE
FROM optionsettranslations ost
  USING optionset os
WHERE
  os.optionsetid = ost.optionsetid AND
  os.name LIKE '%GL -%';


DELETE
FROM optionvaluetranslations ovt
  USING optionvalue ov
    INNER JOIN optionset os ON os.optionsetid = ov.optionsetid
WHERE
  ovt.optionvalueid = ov.optionvalueid AND
  os.name LIKE '%GL -%';


DELETE
FROM optionattributevalues oav
  USING optionvalue ov
    INNER JOIN optionset os ON os.optionsetid = ov.optionsetid
WHERE
  oav.optionvalueid = ov.optionvalueid AND
  os.name LIKE '%GL -%';


DELETE
FROM optionvalue ov
  USING optionset os
WHERE
  os.optionsetid = ov.optionsetid AND
  os.name LIKE '%GL -%';


DELETE
FROM optionsetusergroupaccesses osuga
  USING optionset os
WHERE
  os.optionsetid = osuga.optionsetid AND
  os.name LIKE '%GL -%';


DELETE
FROM optionset os
WHERE
  name LIKE '%GL -%' AND
  uid <> 'lCvUEVCum1A' AND
  uid <> 'COw39TH1LiZ' AND
  uid <> 'FTbwlOo7CpG' AND
  uid <> 'tDxpzAER9vr' AND
  uid <> 'RpW3aZrlHDi' AND
  uid <> 'G69FtaNkBgp' AND
  uid <> 'qr9jBtm9uvm' AND
  uid <> 'bj4Nk1trMYk' AND
  uid <> 'sBdHjBK3WCp' AND
  uid <> 'Y0XGyo88TdG' AND
  uid <> 'DNOavthBRGL' AND
  uid <> 'Z2aTRTVP2OU' AND
  uid <> 'x3qubWeAt1f';


