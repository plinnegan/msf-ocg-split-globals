

def print_section(heading_type, text, logger):
    """Print headings with same width regardless of text inside"""
    types = {
        'program': '=',
        'heading': '=',
        'programStage': '-',
        'subHeading': '-',
        'dataElements': '.',
        'optionSets': '.',
        'subSubHeading': '.'
    }
    symbol_count = 60 - len(text)//2
    symbol_str = ''.join(types[heading_type]*symbol_count)
    output = '{} {} {}'.format(symbol_str, text, symbol_str)
    if len(text) % 2 == 0:
        output += types[heading_type]
    logger.info(output)


def drill_to_data(source, path_to_data):
    current_data = source
    for key in path_to_data:
        if key not in current_data:
            return False
        else:
            current_data = current_data[key]
    return current_data
