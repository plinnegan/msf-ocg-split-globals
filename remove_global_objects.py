from dhis2 import Dhis, setup_logger, logger
import json
import sys

DISH_FILE = '/opt/dhis2/dishLiveMSF.json'


def remove_gl_psdes(api, de_uid_maps, os_uid_maps):
    des_removed = {}
    logger.info('Deleting old global data elements')
    params = {'fields': 'id,name'}
    program_info = [(prg['id'], prg['name']) for prg in api.get('programs', params=params).json()['programs']]
    for prg_uid, prg_name in program_info:
        if prg_uid not in de_uid_maps['dataElements']:
            continue
        des_removed[prg_uid] = {}
        logger.info(f'Processing program {prg_name}')
        p_req = api.get(f'programs/{prg_uid}/metadata')
        ps_data = {'programStages': p_req.json()['programStages']}
        deleted = 0
        for ps in ps_data['programStages']:
            check_options = True
            if ps['id'] not in de_uid_maps['dataElements'][prg_uid]:
                continue
            des_removed[prg_uid][ps['id']] = []
            logger.info(f'Processing program stage {ps["name"]}')
            de_map = de_uid_maps['dataElements'][prg_uid][ps['id']]
            try:
                os_map = os_uid_maps['optionSets'][prg_uid][ps['id']]
            except KeyError:
                check_options = False
            ps_de_count = len(ps['programStageDataElements'])
            psde_new = []
            for i, ps_de in enumerate(ps['programStageDataElements']):
                de = ps_de['dataElement']
                de_uid = de['id']
                if de_uid in de_map:
                    logger.info(f'Found global de: {de_uid}, Removing from program stage...')
                    des_removed[prg_uid][ps['id']].append(de_uid)
                    deleted += 1
                else:
                    psde_new.append(ps_de)
                    if 'optionSet' in de and check_options:
                        os = de['optionSet']
                        if os['id'] in os_map:
                            print('Found GL option set')
                sys.stdout.write(f'\rChecked de {i+1}/{ps_de_count}')
                sys.stdout.flush()
            ps['programStageDataElements'] = psde_new

        p_update_req = api.post('metadata', json=ps_data)
        logger.info(f'PRG: {prg_uid} deleted {deleted} des with code: {p_update_req.status_code}')
    return des_removed


def remove_gldes_from_degroup(api, deg_uid):
    """GL DEs are assigned to a DEG, remove from here before deleting"""
    params = {'fields': 'id,name,dataElements[id,name]'}
    deg_req = api.get(f'dataElementGroups/{deg_uid}', params=params)
    deg_data = deg_req.json()
    deg_nm = deg_data['name']
    de_data = deg_data['dataElements']
    removing = {'deletions': []}
    for de in de_data:
        if 'GL -' in de['name']:
            removing['deletions'].append({'id': de['id']})
    cnt = len(removing["deletions"])
    de_remove_req = api.post(f'dataElementGroups/{deg_uid}/dataElements', data=removing)
    logger.info(f'Deleted {cnt} from dataElementGroup {deg_nm} ({de_remove_req.status_code})')


def delete_gl_meta(api, meta_type):
    params = {
        'fields': 'id,name',
        'filter': 'name:like:GL -',
        'paging': 'false'
    }
    data_req = api.get(meta_type, params=params)
    data = data_req.json()[meta_type]
    for meta_data in data:
        print(f'Deleting {meta_data["name"]}...')
        del_req = api.delete(f'{meta_type}/{meta_data["id"]}')
        logger.info(f'Deleted DE: {meta_data["name"]} ({del_req.status_code})')
    logger.info(f'SUMMARY: Deleted {len(data)} {met_type}.')


if __name__ == "__main__":
    setup_logger('logs/logs_deletion.log', include_caller=False)
    api = Dhis.from_auth_file(DISH_FILE)
    with open('data/GL-dataElements-uid_maps.json', 'r') as f_de:
        with open('data/GL-optionSets-uid_maps.json', 'r') as f_op:
            de_map_data = json.load(f_de)
            os_map_data = json.load(f_op)
            des_removed = remove_gl_psdes(api, de_map_data, os_map_data)
            # with open('deleted_des.json', 'w') as f:
            #     json.dump(des_removed, f, indent=2)
            #     f.close()
            f_op.close()
        f_de.close()
    remove_gldes_from_degroup(api, 'mQswioqgU79')
    # delete_gl_meta(api, 'dataElements')
    # delete_gl_meta(api, 'optionSets')


