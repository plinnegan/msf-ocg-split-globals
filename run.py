from dhis2 import Dhis, setup_logger, logger
from dhis2.exceptions import APIException
import re
import json
from datetime import datetime
from update_de_sql import update_sql
from project_utils import print_section, drill_to_data


DISH_FILE = '/opt/dhis2/dishLiveMSFdev.json'


def delete_metadata(api, data):
    """Delete all metadata speficied in data by its endpoint"""
    for endpoint in data:
        for datum in data[endpoint]:
            del_req = api.delete('{}/{}'.format(endpoint, datum['id']))
            logger.info('{} {} deleted with status code {}'.format(endpoint[:-1], datum['name'], del_req.status_code))


def replace_prop(data, map_info, new_uid):
    """Replace keys in data with the corresponding key value in map_info"""
    properties_to_map = ['name', 'shortName', 'code', 'description']
    for prop in properties_to_map:
        if prop in data:
            if prop in map_info:
                map_prop = prop
            else:
                map_prop = 'shortName'
            if prop == 'code':
                data['code'] += map_info['code']
            else:
                data[prop] = data[prop].replace('GL -', map_info[map_prop])
    data['id'] = new_uid
    now = datetime.strftime(datetime.now(), '%Y-%m-%dT%H:%M:%S.%f')[:-3]
    data['created'] = now
    data['lastUpdated'] = now
    return data


def does_metadata_exists(api, endpoint, name):
    """Check if metadata at specified endpoint with specified name exists"""
    params = {
        'filter': 'name:eq:{}'.format(name),
        'fields': '*'
    }
    data_req = api.get(endpoint, params=params)
    data = data_req.json()[endpoint]
    if len(data) == 0:
        return False, None
    else:
        return True, data[0]


def update_uid_map(uid_map, endpoint, prog, prog_stg, old_uid, new_uid):
    """Add new program, programStage de old -> de new mapping"""
    if prog not in uid_map[endpoint]:
        uid_map[endpoint][prog] = {}
    if prog_stg not in uid_map[endpoint][prog]:
        uid_map[endpoint][prog][prog_stg] = {}
    uid_map[endpoint][prog][prog_stg][old_uid] = new_uid
    return uid_map


def replace_from_name(api, endpoint, name, prop, find_str, replacement_str):
    params = {
        'filter': 'name:eq:{}'.format(name),
        'fields': '*'
    }
    req = api.get(endpoint, params=params)
    data = req.json()[endpoint][0]
    data[prop] = data[prop].replace(find_str, replacement_str)
    put_req = api.put(f'{endpoint}/{data["id"]}', json=data)
    logger.info(f'Updated {data["name"]} status: {put_req.status_code}')


def pre_process_de(api):
    """Perform pre-processing on problematic des so update runs more smoothly"""
    replace_from_name(api, 'dataElements', 'GL - Time of discharge (HH:MM)', 'shortName', 'GL-', 'GL -')
    replace_from_name(api, 'dataElements', 'GL - Seen in MSF nut program during last 2 mnths', 'shortName', 'ATFC -', 'GL -')
    replace_from_name(api, 'dataElements', "GL - Patient coming from... - if 'other', specify", 'shortName',
                      "GL - Patient coming from... - if 'other', specify", "GL - Patient coming from.. if 'other' specify")
    replace_from_name(api, 'dataElements', "GL - Final main diagnosis - if 'other', specify:", 'shortName',
                      "GL - Final main diagnosis - if 'other', specify:", "GL - Final main diagnosis - if 'other' specify")


def set_os_to_gl(api):
    """Set all GL dataElement option sets to have GL in the name
    This way they will be picked up later an new optionSets created"""
    params = {
        'fields': 'id,name,optionSet[*]',
        'filter': 'name:like:GL -',
        'paging': 'false'
    }
    de_req = api.get('dataElements', params=params)
    de_data = de_req.json()['dataElements']
    for gl_de in de_data:
        if 'optionSet' in gl_de:
            de_nm = gl_de['name']
            os = gl_de['optionSet']
            nm = os['name']
            if 'GL -' not in nm:
                suffix = nm[nm.rfind('-'):]
                os['name'] = f'GL {suffix}'
                os_put_req = api.put(f'optionSets/{os["id"]}', json=os)
                logger.info(f'Changed GL DE {de_nm} OS name: {nm}  -> {os["name"]} ({os_put_req.status_code})')





def create_new_values(api, endpoint, uid, map_info):
    req = api.get(f'{endpoint}/{uid}')
    data = req.json()
    old_nm = data['name']
    new_uid = api.generate_uids(1)[0]
    new_data = replace_prop(data, map_info, new_uid)
    if 'options' in new_data:
        update_req = generate_new_os(api, new_uid, new_data)
        print(f'Options updated with code: {update_req.status_code}')
    else:
        update_req = api.post(endpoint, json=new_data)
    logger.info(f'Updated {old_nm} -> {new_data["name"]} | status: {update_req.status_code}')
    return new_data


def generate_new_os(api, new_uid, new_data):
    old_options = json.loads(json.dumps(new_data['options']))
    new_data['options'] = []
    new_data_req = api.post('optionSets', json=new_data)
    print(f'Generating {len(old_options)} new option UIDs for option set {new_data["name"]}')
    for i, opt in enumerate(old_options):
        old_uid = opt['id']
        opt_full = api.get(f'options/{old_uid}').json()
        opt_full['id'] = api.generate_uids(1)[0]
        opt_full['optionSet'] = new_data
        schema_op_req = api.post('schemas/option', {'name': opt_full['name'], 'code': opt_full['code']})
        print(f'Updated option schema with status code {schema_op_req.status_code}')
        new_opt_req = api.post('options', json=opt_full)
        print(f'Created new option with status code {new_opt_req.status_code}')
        new_data['options'].append(json.loads(json.dumps(opt_full)))
    os_update_req = api.put(f'optionSets/{new_uid}', params={'mergeMode': 'REPLACE'}, json=new_data)
    return os_update_req


def process_global(api, endpoint, summary, prog, prg_stg, gl_metadata, map_info, data_store):
    summary['found'] += 1
    uid = gl_metadata['id']
    nm = gl_metadata['name']
    print_section(endpoint, f'Updating {endpoint[:-1]}: {nm}', logger)
    new_nm = nm.replace('GL -', map_info['name'])
    exists, exist_data = does_metadata_exists(api, endpoint, new_nm)
    if exists:
        exist_uid = exist_data['id']
        logger.info(f'{endpoint[:-1]} already exists with uid: {exist_uid}')
        update_uid_map(data_store['uid_maps'], endpoint, prog, prg_stg, uid, exist_uid)
        data_store['new_metadata'][endpoint].append(exist_data)
        summary['existed'] += 1
    else:
        try:
            new_data = create_new_values(api, endpoint, uid, map_info)
            update_uid_map(data_store['uid_maps'], endpoint, prog, prg_stg, uid, new_data['id'])
            summary['created'] += 1
        except APIException as e:
            logger.error(f'ERROR creating new {endpoint[:-1]}')
            logger.error(map_info)
            logger.error(f'{nm} -> {new_nm}')
            logger.error(e)


def global_meta_update(api, prg_map, api_fields, endpoint, path_to_data):
    params = {
        'fields': api_fields,
        'paging': 'false'
    }
    data_store = {
        'uid_maps': {endpoint: {}},
        'new_metadata': {endpoint: []}
    }
    prog_req = api.get('programs', params=params)
    prog_data = prog_req.json()['programs']
    prog_no = len(prog_data)
    for i, program in enumerate(prog_data):
        prg_uid = program['id']
        print_section('program', 'Processing Program: {} ({}/{})'.format(program['name'], i+1, prog_no), logger)
        for prg_stg in program['programStages']:
            prg_stg_uid = prg_stg['id']
            summary = {'found': 0, 'created': 0, 'existed': 0, 'type': endpoint}
            print_section('programStage', 'Processing Program Stage: {}'.format(prg_stg['name']), logger)
            map_info = prg_map[program['id']][prg_stg['id']]
            for ps_de in prg_stg['programStageDataElements']:
                tgt_data = drill_to_data(ps_de, path_to_data)
                if tgt_data and 'GL -' in tgt_data['name']:
                    process_global(api, endpoint, summary, prg_uid, prg_stg_uid, tgt_data, map_info, data_store)
            logger.info('Found {found} global {type} ({created} created, {existed} already existed)'.format(**summary))
    save_data(data_store, endpoint)


def global_de_update(api, prg_map):
    pre_process_de(api)
    set_os_to_gl(api)
    # fields = 'id,name,shortName,programType,programStages[id,name,programStageDataElements[dataElement[id,name,shortName,code,description]]'
    # global_meta_update(api, prg_map, fields, 'dataElements', ['dataElement'])


def global_tea_update(api):
    """Because there are only two global teas and they are both assigned to surgery,
    only a re-naming is required doing it fairly manually"""
    params = {
        'filter': 'name:like:GL - Age in ',
        'fields': '*'
    }
    print_section('program', 'Processing TEAs', logger)
    age_req = api.get('trackedEntityAttributes', params=params)
    age_data = age_req.json()['trackedEntityAttributes']
    for age_tea in age_data:
        old_nm = age_tea['name']
        age_tea['name'] = old_nm.replace('GL -', 'SUR -')
        new_age_req = api.put(f'trackedEntityAttributes/{age_tea["id"]}', json=age_tea)
        logger.info(f'Updated tea ({old_nm} -> {age_tea["name"]}) status code: {new_age_req.status_code}')


def global_option_sets_update(api, prg_map):
    fields = 'id,name,shortName,programType,programStages[id,name,programStageDataElements[dataElement[id,name,optionSet[id,name,options[id,name,code]]]]]'
    global_meta_update(api, prg_map, fields, 'optionSets', ['dataElement', 'optionSet'])


def save_data(json_data, endpoint):
    for data_type in ['uid_maps', 'new_metadata']:
        with open(f'data/GL-{endpoint}-{data_type}.json', 'w') as f:
            json.dump(json_data[data_type], f, indent=2)
            f.close()


def global_prog_inds_update(api, de_uid_maps):
    print_section('program', 'Processing Program Indicators', logger)
    matched_des = {}
    params = {
        'fields': 'id,name,shortName,expression,filter,program[id,name]',
        'paging': 'false'
    }
    p = re.compile('\.[a-zA-Z0-9]{11}\}')
    prog_ind_req = api.get('programIndicators', params=params)
    prog_ind_data = prog_ind_req.json()['programIndicators']
    logger.info('Got {} programIndicators'.format(len(prog_ind_data)))
    updated = 0
    for prog_ind in prog_ind_data:
        program = prog_ind['program']['id']
        program_nm = prog_ind['program']['name']
        pi_uid = prog_ind['id']
        pi_name = prog_ind['name']
        formula_updated = False
        for formula in ['filter', 'expression']:
            if formula not in prog_ind:
                continue
            pi_formula = prog_ind[formula]
            formula_des = [expr[1:-1] for expr in p.findall(pi_formula)]
            for de_uid in formula_des:
                if de_uid not in matched_des:
                    de_name = api.get('dataElements/{}'.format(de_uid)).json()['name']
                    if 'GL -' in de_name:
                        matched_des[de_uid] = de_name
                    else:
                        matched_des[de_uid] = False
                if matched_des[de_uid]:
                    old_de_name = matched_des[de_uid]
                    new_uid = find_program_specific_de(de_uid, program, de_uid_maps)
                    if new_uid:
                        new_de = api.get(f'dataElements/{new_uid}').json()
                        pi_formula = pi_formula.replace(de_uid, new_uid)
                        formula_updated = True
                    else:
                        logger.info(f'DE: {de_uid} not found in program map: {program}')
            prog_ind[formula] = pi_formula
        if formula_updated:
            pi_put_req = api.put(f'programIndicators/{pi_uid}', json=prog_ind)
            updated += 1
            logger.info((f'PROGRAM: {program_nm} | Updated {pi_name} for de {old_de_name} TO {new_de["name"]}'
                   f'status code: {pi_put_req.status_code}'))
    if updated > 0:
        logger.info(f'Updated {updated} program rule(s)')
    else:
        logger.info('No program indicators required updating')


def global_prg_rul_var_update(api, de_uid_maps):
    print_section('program', 'Processing Program Rule Variables', logger)
    params = {
        'fields': 'id,name,programRuleVariableSourceType,program[id,name],trackedEntityAttribute[id,name],dataElement[id,name]',
        'paging': 'false'
    }
    prog_rul_var_req = api.get('programRuleVariables', params=params)
    prog_rul_var_data = prog_rul_var_req.json()['programRuleVariables']
    logger.info(f'Found {len(prog_rul_var_data)} program rule variables')
    updated_prvs = 0
    for prog_rul_var in prog_rul_var_data:
        if prog_rul_var['programRuleVariableSourceType'] == 'TEI_ATTRIBUTE':
            continue
        if 'dataElement' not in prog_rul_var:
            logger.warning('Found empty program rule')
            continue
        prv_uid = prog_rul_var['id']
        prv_name = prog_rul_var['name']
        de = prog_rul_var['dataElement']
        if 'GL -' in de['name']:
            program = prog_rul_var['program']['id']
            program_nm = prog_rul_var['program']['name']
            de_uid = de['id']
            new_uid = find_program_specific_de(de_uid, program, de_uid_maps)
            if new_uid:
                new_de = api.get(f'dataElements/{new_uid}').json()
                prog_rul_var['dataElement'] = new_de
                prv_put_req = api.put(f'programRuleVariables/{prv_uid}', json=prog_rul_var)
                updated_prvs += 1
                logger.info((f'PROGRAM: {program_nm} | Updated {prv_name} for de {de["name"]} to {new_de["name"]}'
                       f' status code: {prv_put_req.status_code}'))
            else:
                logger.info(f'DE: {de_uid} not found in program map')
    if updated_prvs > 0:
        logger.info(f'Updated {updated_prvs} program rule(s)')
    else:
        logger.info('No program rule variables required updating')


def find_program_specific_de(uid, prg, de_map):
    prg_data = de_map['dataElements'][prg]
    for prg_stg in prg_data:
        if uid in prg_data[prg_stg]:
            return prg_data[prg_stg][uid]
    return False


def global_prog_rules_update(api, de_uid_maps):
    print_section('program', 'Processing Program Rules', logger)
    program_info = [(prg['id'], prg['name']) for prg in
        api.get('programs', params={'fields': 'id,name'}).json()['programs']]
    for prg_uid, prg_nm in program_info:
        print_section('program', f'Processing program {prg_nm}', logger)
        if prg_uid not in de_uid_maps['dataElements']:
            continue
        p_req = api.get(f'programs/{prg_uid}/metadata')
        p_data = p_req.json()
        new_p_data, updated = update_prs(api, p_data, de_uid_maps)
        if updated > 0:
            updated_p_data = {'programRules': new_p_data['programRules'],
                              'programRuleActions': new_p_data['programRuleActions']}
            p_update_req = api.post('metadata', json=updated_p_data)
            logger.info(f'Updated {updated} programRules in {prg_nm} ({p_update_req.status_code})')
        else:
            logger.info(f'No program rules required updating in {prg_nm}')


def update_prs(api, p_data, de_uid_maps):
    matched_des = {}
    updated = 0
    pr_data = p_data['programRules']
    pra_data = p_data['programRuleActions']
    pr_action_params = {'fields': 'id,programRuleActionType,programRule[id,name],dataElement[id,name]'}
    for prog_rule in pr_data:
        pr_nm = prog_rule['name']
        program = prog_rule['program']['id']
        for i, action_id in enumerate(prog_rule['programRuleActions']):
            action = api.get(f'programRuleActions/{action_id["id"]}', params=pr_action_params).json()
            if 'dataElement' not in action:
                continue
            de_uid = action['dataElement']['id']
            if de_uid not in matched_des:
                de_name = api.get('dataElements/{}'.format(de_uid)).json()['name']
                if 'GL -' in de_name:
                    matched_des[de_uid] = de_name
                else:
                    matched_des[de_uid] = False
            if matched_des[de_uid]:
                old_de_name = matched_des[de_uid]
                new_uid = find_program_specific_de(de_uid, program, de_uid_maps)
                if new_uid:
                    new_de = api.get(f'dataElements/{new_uid}').json()
                    action['dataElement'] = new_de
                    prog_rule['programRuleActions'][i] = action
                    update_pr_rule_action(pra_data, de_uid, new_de)
                    updated += 1
                    logger.info(f'Updating {pr_nm} for de {old_de_name}  ->  {new_de["name"]}')
                else:
                    logger.info(f'DE: {de_uid} not found in program map')
    return p_data, updated


def update_pr_rule_action(action_data, old_de_uid, new_de):
    for action in action_data:
        if 'dataElement' in action and action['dataElement']['id'] == old_de_uid:
            action['dataElement'] = new_de
            break
    return action_data


def apply_de_updates(api, de_uid_maps):
    print_section('program', 'Assigning new data elements', logger)
    program_info = [(prg['id'], prg['name']) for prg in api.get('programs', params={'fields': 'id,name'}).json()['programs']]
    for prg_uid, prg_nm in program_info:
        if prg_uid not in de_uid_maps['dataElements']:
            continue
        p_map = de_uid_maps['dataElements'][prg_uid]
        p_req = api.get(f'programs/{prg_uid}/metadata')
        p_data = p_req.json()
        with open(f'data/program-{prg_uid}.json', 'w') as f:
            json.dump(p_data, f, indent=2)
            f.close()
        ps_data = {'programStages': p_data['programStages']}
        update_psdes(api, ps_data, p_map)
        ps_section_data = {'programStageSections': p_data['programStageSections']}
        update_ps_sections(api, ps_section_data, p_map)


def update_psdes(api, ps_data, p_map):
    prg_nm = ps_data['programStages'][0]['program']['id']
    updated = 0
    for ps in ps_data['programStages']:
        if ps['id'] not in p_map:
            continue
        ps_de_map = p_map[ps['id']]
        for ps_de in ps['programStageDataElements']:
            de_uid = ps_de['dataElement']['id']
            de_nm = api.get(f'dataElements/{de_uid}').json()['name']
            if de_uid in ps_de_map:
                new_uid = ps_de_map[de_uid]
                ps_de_copy = json.loads(json.dumps(ps_de))
                ps_de_copy['dataElement'] = {'id': new_uid}
                new_psde_uid = api.generate_uids(1)[0]
                ps_de_copy['id'] = new_psde_uid
                ps['programStageDataElements'].append(ps_de_copy)
                updated += 1
            elif 'GL -' in de_nm:
                logger.info(f'{de_nm} is not in ps_de_map')
    p_update_req = api.post('metadata', json=ps_data)
    logger.info(f'PRG: {prg_nm} added {updated} des with code: {p_update_req.status_code}')


def update_ps_sections(api, ps_section_data, p_map):
    swapped = 0
    for ps_section in ps_section_data['programStageSections']:
        ps_uid = ps_section['programStage']['id']
        if ps_uid not in p_map:
            continue
        ps_de_map = p_map[ps_uid]
        new_section_des = []
        section_nm = ps_section['name']
        for de in ps_section['dataElements']:
            if de['id'] in ps_de_map:
                new_uid = ps_de_map[de['id']]
                new_section_des.append({'id': new_uid})
                swapped += 1
            else:
                new_section_des.append({'id': de['id']})
        ps_section['dataElements'] = new_section_des
    p_update_req = api.post('metadata', json=ps_section_data)
    logger.info(f'SECTION: {section_nm} swapped {swapped} des in section form with code: {p_update_req.status_code}')


def apply_os_updates(api, os_uid_maps):
    print_section('program', 'Assigning new option sets to des', logger)
    specific_os_updates(api, os_uid_maps)
    params = {
        'fields': 'id,name,shortName,programType,programStages[id,name,programStageDataElements[dataElement[id,name,optionSet[id,name,options[id,name]]]]',
        'paging': 'false'
    }
    prg_req = api.get('programs', params=params)
    for prg in prg_req.json()['programs']:
        p_uid = prg['id']
        p_nm = prg['name']
        if p_uid not in os_uid_maps['optionSets']:
            continue
        p_map = os_uid_maps['optionSets'][p_uid]
        for prg_stg in prg['programStages']:
            ps_uid = prg_stg['id']
            ps_nm = prg_stg['name']
            if ps_uid not in p_map:
                continue
            ps_os_map = p_map[ps_uid]
            for ps_de in prg_stg['programStageDataElements']:
                de = ps_de['dataElement']
                de_nm = de['name']
                if 'optionSet' in de:
                    os = de['optionSet']
                    os_nm = os['name']
                    if 'GL -' in os_nm:
                        if os['id'] in ps_os_map:
                            de_full = api.get(f'dataElements/{de["id"]}').json()
                            new_os_uid = ps_os_map[os['id']]
                            new_os = api.get(f'optionSets/{new_os_uid}').json()
                            new_os_nm = new_os['name']
                            de_full['optionSet'] = new_os
                            update_os_req = api.put(f'dataElements/{de["id"]}', json=de_full)
                            logger.info(f'PR: {p_nm} | PS: {ps_nm} | DE: {de_nm} | OS: {os_nm} -> {new_os_nm} status code: {update_os_req.status_code}')
                        else:
                            logger.info(f'{os_nm} ({os["id"]}) is not in os map')


def specific_os_updates(api, os_uid_maps):
    """Some data elements with GL option sets are not assigned to any programs,
    so they are missed by the main code, update these more manually"""
    ps_os_map = os_uid_maps['optionSets']['Xh3piNNdT6x']['M03l72XbRqp']
    problem_des = ['JkRpQ7yHyT3', 'Tdm8OP4NKlP', 'o3I50BJy84v']
    params = {'fields': '*,optionSet[id,name]'}
    for de_uid in problem_des:
        de = api.get(f'dataElements/{de_uid}', params=params).json()
        os_nm = de['optionSet']['name']
        os_uid = de['optionSet']['id']
        if os_uid in ps_os_map:
            new_os_uid = ps_os_map[os_uid]
            new_os = api.get(f'optionSets/{new_os_uid}').json()
            de['optionSet'] = new_os
            os_update_req = api.put(f'dataElements/{de["id"]}', json=de)
            logger.info(f'Updated de {de["name"]} OS: {os_nm}  ->  {new_os["name"]} ({os_update_req.status_code})')
        else:
            logger.warning(f'Option set {os_nm} not in os map for program Medical - Pediatrics/Nutrition')


def reset_programs(api):
    import glob
    for program_file in glob.glob('data/program-*.json'):
        with open(program_file, 'r') as f:
            prg_data = json.load(f)
            prg_nm = prg_data['programs'][0]['name']
            prg_reset_req = api.post('metadata', json=prg_data)
            logger.info(f'Reset {prg_nm} with status code: {prg_reset_req.status_code}')
            f.close()


def run():
    setup_logger('logs/logs.log', include_caller=False)
    api = Dhis.from_auth_file(DISH_FILE)

    # reset_programs(api)

    with open('program_name_conventions.json', 'r') as f:
        program_naming_conventions = json.load(f)
        global_de_update(api, program_naming_conventions)
        # global_tea_update(api)
        # global_option_sets_update(api, program_naming_conventions)
        f.close()

    with open('data/GL-dataElements-uid_maps.json', 'r') as f:
        de_map_data = json.load(f)
        # apply_de_updates(api, de_map_data)
        # global_prg_rul_var_update(api, de_map_data)
        # global_prog_inds_update(api, de_map_data)
        # global_prog_rules_update(api, de_map_data)
        f.close()

    with open('data/GL-optionSets-uid_maps.json', 'r') as f:
        os_map_data = json.load(f)
        # apply_os_updates(api, os_map_data)
        f.close()

    # update_sql()


if __name__ == "__main__":
    run()
