from dhis2 import Dhis, setup_logger, logger
from dhis2.exceptions import APIException
from project_utils import print_section
import json
import re

DISH_FILE = '/opt/dhis2/dishLocalMSF.json'


def replace_if_exists(api, de_uid, de_map):
    if de_uid in de_map:
        new_uid = de_map[de_uid]
        new_de = api.get(f'dataElements/{new_uid}').json()
        return new_de
    else:
        return False


def alert_gl_de_uid_list(api, data_list, de_map, uid_path_fn, fav_nm):
    uid_regex = re.compile('[a-zA-Z0-9]{11}')
    for datum in data_list:
        uid = uid_path_fn(datum)
        if not uid_regex.fullmatch(uid):
            continue
        try:
            de = api.get(f'dataElements/{uid}').json()
            old_nm = de['name']
            new_de = replace_if_exists(api, uid, de_map)
            if new_de:
                print(f'{fav_nm} | Replace {old_nm}  ->  {new_de["name"]}')
            elif 'GL -' in old_nm:
                logger.warning(f'{fav_nm} | GL dataElement: {old_nm} found but not in map.')
        except APIException:
            try:
                meta_obj = api.get(f'identifiableObjects/{uid}').json()
                meta_type = meta_obj['href'].split('/')[4]
                print(f'UID {uid} is a {meta_type[:-1]}, not processing')
                if meta_type == 'optionSets':
                    print(f'Found optionSet: {meta_obj["name"]} ({uid})')
            except APIException:
                print(f'{uid} is not a valid object UID')


def alert_gl_in_obj(api, prop_fn_dict, data, de_map, fav_nm):
    for prop in prop_fn_dict:
        fn_to_uid = prop_fn_dict[prop]
        alert_gl_de_uid_list(api, data[prop], de_map, fn_to_uid, fav_nm)


def global_dash_update(api, de_uid_map, endpoint):
    params = {
        'fields': '*,dataElementDimensions[dataElement[id,name]]',
        'paging': 'false'
    }
    data_req = api.get(endpoint, params=params)
    data = data_req.json()[endpoint]
    item_no = len(data)
    for i, dash_item in enumerate(data, 1):
        d_nm = dash_item['name']
        print_section('heading', f'Processing analytic item {d_nm} ({i}/{item_no})', logger)
        if de_uid_map:
            prg = dash_item['program']
            ps = dash_item['programStage']
            full_map = de_uid_map['dataElements']
            if prg['id'] not in full_map:
                continue
            if ps['id'] not in full_map[prg['id']]:
                continue
            de_map = de_uid_map['dataElements'][prg['id']][ps['id']]
        else:
            de_map = {}
        path_fn_dict = {
            'dataElementDimensions': lambda x: x['dataElement']['id'],
            'columnDimensions': lambda x: x,
            'rowDimensions': lambda x: x,
            'filterDimensions': lambda x: x
        }
        alert_gl_in_obj(api, path_fn_dict, dash_item, de_map, d_nm)



def run():
    setup_logger('logs/logs_dashboard.log', include_caller=False)
    api = Dhis.from_auth_file(DISH_FILE)
    with open('data/GL-dataElements-uid_maps.json', 'r') as f:
        de_map_data = json.load(f)
        global_dash_update(api, de_map_data, 'eventCharts')
        global_dash_update(api, {}, 'reportTables')


if __name__ == "__main__":
    run()