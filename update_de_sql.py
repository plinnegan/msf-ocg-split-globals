import json


def generate_sql(sql_template_str, uid_map_dict):
    result = []
    subs = {
        'new_uid': '',
        'old_uid': '',
        'program_stage': '',
        'program': ''
    }
    for program_uid in uid_map_dict['dataElements']:
        subs['program'] = f'\'{program_uid}\''
        ps_uids = uid_map_dict['dataElements'][program_uid]
        for program_stage_uid in ps_uids:
            subs['program_stage'] = f'\'{program_stage_uid}\''
            des = ps_uids[program_stage_uid]
            for old_de in des:
                new_de = des[old_de]
                subs['old_uid'] = f'\'{old_de}\''
                subs['new_uid'] = f'\'{new_de}\''
                result.append(sql_template_str.format(**subs)+'\n\n')
    return result


def update_sql():
    with open('update_de_values_template.sql', 'r') as tmpltf:
        with open('data/GL-dataElements-uid_maps.json', 'r') as mapf:
            sql_template_str = ''.join(tmpltf.readlines())
            uid_map = json.load(mapf)
            sql_list = generate_sql(sql_template_str, uid_map)
            with open('update_de_values.sql', 'w') as sqlf:
                sqlf.writelines(sql_list)
                sqlf.close()
            mapf.close()
        tmpltf.close()


if __name__ == "__main__":
    update_sql()

