UPDATE trackedentitydatavalue tedv
SET dataelementid = (
  SELECT
    de.dataelementid
  FROM dataelement de
  WHERE de.uid = {new_uid}
)
FROM programstageinstance psi
  INNER JOIN programstage ps on psi.programstageid = ps.programstageid
  INNER JOIN program p on ps.programid = p.programid
WHERE
  psi.programstageinstanceid = tedv.programstageinstanceid AND
  tedv.dataelementid = (
    SELECT
      de.dataelementid
    FROM dataelement de
    WHERE de.uid = {old_uid}
  ) AND
  ps.uid = {program_stage} AND
  p.uid = {program};
